﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPi.Models;

namespace WebAPi.Controllers
{
    [Route("api/Viajes")]
    [ApiController]
    public class ViajesController : ControllerBase
    {
        private readonly ViajesEtechContext _context;

        /* Contructor de la clase*/
        public ViajesController(ViajesEtechContext context)
        {
            _context = context;
        }

        // Ejejmplo de llamda GET: api/Viajes
        /* Metodo que permite obtener todos los viajes
         */
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Viaje>>> GetViaje()
        {
            return await _context.Viaje.ToListAsync();
        }


        //Ejemplo de la llamda GET: api/Viajes/5
        /* Metodo que permite obtener un viaje dado como parametro 
         * el identificador
         * */
        [HttpGet("{id}")]
        public async Task<ActionResult<Viaje>> GetViaje(int id)
        {
            var viaje = await _context.Viaje.FindAsync(id);

            if (viaje == null)
            {
                return NotFound();
            }

            return viaje;
        }

        // PUT: api/Viajes/5
        /* Metodo que permite modificar un viaje, recibe como parametro
         * el identificador del viaje a modificar y el objeto viaje modificado
         * */
        [HttpPut("{id}")]
   
        public async Task<ActionResult<Viaje>> PutViaje(int id, [FromBody]Viaje viaje)
        {
            if (id != viaje.Id)
            {
                return BadRequest();
            }

            _context.Entry(viaje).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViajeExists(viaje.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            //return NoContent();
            return CreatedAtAction(nameof(GetViaje), new { id = viaje.Id }, viaje);
        }

        // Ejemplo de llamada POST: api/Viajes
        /* Metodo que permite crear un viaje, recibe como parametro el 
         * viaje nuevo a ingresar*/
        [HttpPost]
        public async Task<ActionResult<Viaje>> PostViaje(Viaje viaje)
        {
            _context.Viaje.Add(viaje);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ViajeExists(viaje.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction(nameof(GetViaje), new { id = viaje.Id }, viaje);
        }

        //Ejemplo de la llamda DELETE: api/Viajes/5
        /* Metodo que permite eliminar un viaje, recibe como
         * parametro el identificador del viaje a eliminar*/
        [HttpDelete("{id}")]
        public async Task<ActionResult<Viaje>> DeleteViaje(int id)
        {
            var viaje = await _context.Viaje.FindAsync(id);
            if (viaje == null)
            {
                return NotFound();
            }

            _context.Viaje.Remove(viaje);
            await _context.SaveChangesAsync();

            return viaje;
        }

        /* Metodo que permite validar si un viaje existe*/
        private bool ViajeExists(int id)
        {
            return _context.Viaje.Any(e => e.Id == id);
        }
    }
}
