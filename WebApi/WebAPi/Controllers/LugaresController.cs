﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPi.Models;

namespace WebAPi.Controllers
{
    [Route("api/Lugares")]
    [ApiController]
    public class LugaresController : ControllerBase
    {
        private readonly ViajesEtechContext _context;

        /* Constructor
         */
        public LugaresController(ViajesEtechContext context)
        {
            _context = context;
        }


        // Ejemplo de llamda GET: api/Lugares
        /* Metodo para consultar todos los lugares.
         */
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Lugar>>> GetLugar()
        {
            return await _context.Lugar.ToListAsync();
        }

        // Ejemplo de llamada GET: api/Lugares/5
        /* Metodo que permite buscar los lugares, recibe como parametro el identificador
         * del lugar a consultar
         */
        [HttpGet("{id}")]
        public async Task<ActionResult<Lugar>> GetLugar(int id)
        {
            var lugar = await _context.Lugar.FindAsync(id);

            if (lugar == null)
            {
                return NotFound();
            }

            return lugar;
        }

        // Ejejmplo de llamada PUT: api/Lugares/5
        /*Metodo que permite modificar un lugar. Recibe como parametro
         * el id del lugar a modificar y el objeto lugar modificado 
         * con los cambios
         */
        [HttpPut("{id}")]
        public async Task<ActionResult<Lugar>> PutLugar(int id, [FromBody]Lugar lugar)
        {
            if (id != lugar.Id)
            {
                return BadRequest();
            }

            _context.Entry(lugar).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LugarExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction(nameof(GetLugar), new { id = lugar.Id }, lugar);
        }

        // Ejemplo de llamada POST: api/Lugares
        /* Metodo para crear un lugar, recibe como parametro el objeto 
         * lugar el cual se crea con el json con el cual es llamado el metodo del api
         * */
        [HttpPost]
        public async Task<ActionResult<Lugar>> PostLugar(Lugar lugar)
        {
            _context.Lugar.Add(lugar);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (LugarExists(lugar.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetLugar", new { id = lugar.Id }, lugar);
        }

        // Ejemplo de llamada DELETE: api/Lugares/5
        /* Metodo para eliminar lugar, recibe como parametro un int 
         * que corresponde con el identificador del lugar a eliminar
         * */
        [HttpDelete("{id}")]
        public async Task<ActionResult<Lugar>> DeleteLugar(int id)
        {
            var lugar = await _context.Lugar.FindAsync(id);
            if (lugar == null)
            {
                return NotFound();
            }

            _context.Lugar.Remove(lugar);
            await _context.SaveChangesAsync();

            return lugar;
        }

        private bool LugarExists(int id)
        {
            return _context.Lugar.Any(e => e.Id == id);
        }
    }
}
