﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPi.Models;

namespace WebAPi.Controllers
{
    [Route("api/Viajeros")]
    [ApiController]
    public class ViajerosController : ControllerBase
    {
        private readonly ViajesEtechContext _context;
        /* Constructor de clase
         */
        public ViajerosController(ViajesEtechContext context)
        {
            _context = context;
        }

        // Ejemplo de llamada GET: api/Viajeros
        /* Metodo para consultar todos los viajeros.
         */
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Viajero>>> GetViajero()
        {
            return await _context.Viajero.ToListAsync();
        }

        // Ejmeplo de llamada GET: api/Viajeros/5
        /* Metodo que consulta un viajero en especifico. Recibe como 
         * parametro el identificador del viajero
         */
        [HttpGet("{id}")]
        public async Task<ActionResult<Viajero>> GetViajero(int id)
        {
            var viajero = await _context.Viajero.FindAsync(id);

            if (viajero == null)
            {
                return NotFound();
            }

            return viajero;
        }

        // Ejemplo de la llamada PUT: api/Viajeros/5
        /* Metodo que permite editar un viajero. Recibe como parametro
         * el identificador del viajero a editar y el objeto viajero modificado
         */
        [HttpPut("{id}")]
        public async Task<ActionResult<Viajero>> PutViajero(int id, [FromBody]Viajero viajero)
        {
            if (id != viajero.Id)
            {
                return BadRequest();
            }

            _context.Entry(viajero).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViajeroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction(nameof(GetViajero), new { id = viajero.Id }, viajero);
        }

        // Ejemplo de la llamda POST: api/Viajeroes
        /* Metodo que permite crear un viajero. Recibe como parametro el 
         * objeto viajero nuevo
         */
        [HttpPost]
        public async Task<ActionResult<Viajero>> PostViajero(Viajero viajero)
        {
            _context.Viajero.Add(viajero);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ViajeroExists(viajero.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetViajero", new { id = viajero.Id }, viajero);
        }

        //Ejemplo de la llamada DELETE: api/Viajeros/5
        /* Metodo que permite eliminar un viajero. Recibe como parametro el
         * identificador del viajero a eliminar.
         */
        [HttpDelete("{id}")]
        public async Task<ActionResult<Viajero>> DeleteViajero(int id)
        {
            var viajero = await _context.Viajero.FindAsync(id);
            if (viajero == null)
            {
                return NotFound();
            }

            _context.Viajero.Remove(viajero);
            await _context.SaveChangesAsync();

            return viajero;
        }

        /* Metodo que permite validar si un viajero existe
         * Recibe como parametro el id del viajero a validar
         */
        private bool ViajeroExists(int id)
        {
            return _context.Viajero.Any(e => e.Id == id);
        }
    }
}
