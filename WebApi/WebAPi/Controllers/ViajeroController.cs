﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebAPi.Models;

namespace WebAPi.Controllers
{
    [Route("api/Viajero")]
    [ApiController]
    public class ViajeroController : ControllerBase
    {
        private readonly ViajesContext _context;

        public ViajeroController(ViajesContext context)
        {
            _context = context;
        }

        // GET: api/Viajero
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Viajero>>> GetTodoItems()
        {
            return await _context.Viajeros.ToListAsync();
        }

        // GET: api/Viajero/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Viajero>> GetViajero(int id)
        {
            var viajero = await _context.Viajeros.FindAsync(id);

            if (viajero == null)
            {
                return NotFound();
            }

            return viajero;
        }

        // PUT: api/Viajero/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutViajero(int id, Viajero viajero)
        {
            if (id != viajero.Id)
            {
                return BadRequest();
            }

            _context.Entry(viajero).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ViajeroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Viajero
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Viajero>> PostViajero(Viajero viajero)
        {
            _context.Viajeros.Add(viajero);
            await _context.SaveChangesAsync();

            //return CreatedAtAction("GetViajero", new { id = viajero.Id }, viajero);
            return CreatedAtAction(nameof(GetViajero), new { id = viajero.Id }, viajero);
        }

        // DELETE: api/Viajero/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Viajero>> DeleteViajero(int id)
        {
            var viajero = await _context.Viajeros.FindAsync(id);
            if (viajero == null)
            {
                return NotFound();
            }

            _context.Viajeros.Remove(viajero);
            await _context.SaveChangesAsync();

            return viajero;
        }

        private bool ViajeroExists(int id)
        {
            return _context.Viajeros.Any(e => e.Id == id);
        }
    }
}
