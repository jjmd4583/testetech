﻿using System;
using System.Collections.Generic;

namespace WebAPi.Models
{
    public partial class Viajero
    {
        public Viajero()
        {
            ViajeroViaje = new HashSet<ViajeroViaje>();
        }

        public int Id { get; set; }
        public string VCedula { get; set; }
        public string VDireccion { get; set; }
        public string VNombre { get; set; }
        public string VApellido { get; set; }
        public int? VTelefono { get; set; }

        public virtual ICollection<ViajeroViaje> ViajeroViaje { get; set; }
    }
}
