﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebAPi.Models
{
    public partial class ViajesEtechContext : DbContext
    {
        public ViajesEtechContext()
        {
        }

        public ViajesEtechContext(DbContextOptions<ViajesEtechContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Lugar> Lugar { get; set; }
        public virtual DbSet<Viaje> Viaje { get; set; }
        public virtual DbSet<Viajero> Viajero { get; set; }
        public virtual DbSet<ViajeroViaje> ViajeroViaje { get; set; }

   
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Lugar>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.LNombre)
                    .HasColumnName("l_nombre")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Viaje>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ViCodigo)
                    .HasColumnName("vi_codigo")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ViLugarDestinoId).HasColumnName("vi_lugar_destino_id");

                entity.Property(e => e.ViLugarOrigenId).HasColumnName("vi_lugar_origen_id");

                entity.Property(e => e.ViPlazas).HasColumnName("vi_plazas");

                entity.Property(e => e.ViPrecio).HasColumnName("vi_precio");

            });

            modelBuilder.Entity<Viajero>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.VApellido)
                    .HasColumnName("v_apellido")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VCedula)
                    .HasColumnName("v_cedula")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VDireccion)
                    .HasColumnName("v_direccion")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.VNombre)
                    .HasColumnName("v_nombre")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.VTelefono).HasColumnName("v_telefono");
            });

            modelBuilder.Entity<ViajeroViaje>(entity =>
            {
                entity.HasKey(e => new { e.ViajeroviId, e.ViajeId });

                entity.ToTable("Viajero_viaje");

                entity.Property(e => e.ViajeroviId).HasColumnName("viajerovi_id");

                entity.Property(e => e.ViajeId).HasColumnName("viaje_id");

                entity.HasOne(d => d.Viaje)
                    .WithMany(p => p.ViajeroViaje)
                    .HasForeignKey(d => d.ViajeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Viajero_viaje_Viaje");

                entity.HasOne(d => d.Viajerovi)
                    .WithMany(p => p.ViajeroViaje)
                    .HasForeignKey(d => d.ViajeroviId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Viajero_viaje_Viajero");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
