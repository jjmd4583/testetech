﻿using System;
using System.Collections.Generic;

namespace WebAPi.Models
{
    public partial class Viaje
    {
        public Viaje()
        {
            ViajeroViaje = new HashSet<ViajeroViaje>();
        }

        public int Id { get; set; }
        public string ViCodigo { get; set; }
        public int? ViPlazas { get; set; }
        public int? ViPrecio { get; set; }
        public int? ViLugarOrigenId { get; set; }
        public int? ViLugarDestinoId { get; set; }

        public virtual ICollection<ViajeroViaje> ViajeroViaje { get; set; }
    }
}
