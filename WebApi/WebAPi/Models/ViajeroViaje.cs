﻿using System;
using System.Collections.Generic;

namespace WebAPi.Models
{
    public partial class ViajeroViaje
    {
        public int Id { get; set; }
        public int ViajeroviId { get; set; }
        public int ViajeId { get; set; }

        public virtual Viaje Viaje { get; set; }
        public virtual Viajero Viajerovi { get; set; }
    }
}
