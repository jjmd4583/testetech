﻿using Microsoft.EntityFrameworkCore;

namespace WebAPi.Models
{
    public class ViajesContext: DbContext
    {
        public ViajesContext(DbContextOptions<ViajesContext> options)
            : base(options)
        {
        }

        public DbSet<Viajero> Viajeros { get; set; }
    }
}
