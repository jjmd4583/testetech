﻿namespace ViajerosClient.Models
{
    public class Viajero
    {
        public Viajero()
        {
        }
        public int Id { get; set; }
        public string VCedula { get; set; }
        public string VDireccion { get; set; }
        public string VNombre { get; set; }
        public string VApellido { get; set; }
        public int? VTelefono { get; set; }
    }
}
