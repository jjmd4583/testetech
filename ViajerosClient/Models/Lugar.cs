﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ViajerosClient.Models
{
    public class Lugar
    {
        public int Id { get; set; }
        public string LNombre { get; set; }
    }
}
