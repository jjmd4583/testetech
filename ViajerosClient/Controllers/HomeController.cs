﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ViajerosClient.Models;

namespace ViajerosClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> IndexAsync()
        {
            List<Viaje> viajeList = new List<Viaje>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://localhost:5001/api/Viajes"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    viajeList = JsonConvert.DeserializeObject<List<Viaje>>(apiResponse);
                }
            }
            return View(viajeList);
          
        }

        public async Task<IActionResult> UpdateViaje(int id)
        {
            Viaje viaje = new Viaje();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://localhost:5001/api/Viajes/" + id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    viaje = JsonConvert.DeserializeObject<Viaje>(apiResponse);
                }
            }
            return View(viaje);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateViaje(Viaje viaje)
        {   
            Viaje viajeUpdate = new Viaje();
            using (var httpClient = new HttpClient())
            {
                 httpClient.BaseAddress = new Uri("https://localhost:5001");
                
                string serilized = JsonConvert.SerializeObject(viaje);
                var inputMessage = new HttpRequestMessage
                {
                    Content = new StringContent(serilized, Encoding.UTF8, "application/json")
                };
                inputMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (var response = await httpClient.PutAsync("/api/Viajes/" + viaje.Id, inputMessage.Content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ViewBag.Result = "Success";
                    viajeUpdate = JsonConvert.DeserializeObject<Viaje>(apiResponse);
                }
            }
            return View(viajeUpdate);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteViaje(int viajeId)
        {
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.DeleteAsync("https://localhost:5001/api/Viajes/" + viajeId))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                }
            }

            return RedirectToAction("Index");
        }


        public ViewResult AddViaje() => View();

        [HttpPost]
        public async Task<IActionResult> AddViaje(Viaje viaje)
        {
            Viaje newViaje = new Viaje();
            using (var httpClient = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(viaje), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync("https://localhost:5001/api/Viajes", content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    newViaje = JsonConvert.DeserializeObject<Viaje>(apiResponse);
                }
            }
            return View(newViaje);
        }


        public ViewResult GetViaje() => View();

        [HttpPost]
        public async Task<IActionResult> GetViaje(int id)
        {
            Viaje viajeResult = new Viaje();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://localhost:5001/api/Viajes/" + id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    viajeResult = JsonConvert.DeserializeObject<Viaje>(apiResponse);
                }
            }
            return View(viajeResult);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
