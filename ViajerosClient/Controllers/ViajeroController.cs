﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ViajerosClient.Models;

namespace ViajerosClient.Controllers
{
    public class ViajeroController : Controller
    {
        private readonly ILogger<ViajeroController> _logger;

        public ViajeroController(ILogger<ViajeroController> logger) => _logger = logger;

        public async Task<IActionResult> IndexAsync()
        {
            List<Viajero> viajeroList = new List<Viajero>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://localhost:5001/api/Viajeros"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    viajeroList = JsonConvert.DeserializeObject<List<Viajero>>(apiResponse);
                }
            }
            return View(viajeroList);

        }

        public async Task<IActionResult> UpdateViajero(int id)
        {
            Viajero viajero = new Viajero();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://localhost:5001/api/Viajeros/" + id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    viajero = JsonConvert.DeserializeObject<Viajero>(apiResponse);
                }
            }
            return View(viajero);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateViajero(Viajero viajero)
        {
            Viajero viajeroUpdate = new Viajero();
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri("https://localhost:5001");

                string serilized = JsonConvert.SerializeObject(viajero);
                var inputMessage = new HttpRequestMessage
                {
                    Content = new StringContent(serilized, Encoding.UTF8, "application/json")
                };
                inputMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (var response = await httpClient.PutAsync("/api/Viajeros/" + viajero.Id, inputMessage.Content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ViewBag.Result = "Success";
                    viajeroUpdate = JsonConvert.DeserializeObject<Viajero>(apiResponse);
                }
            }
            return View(viajeroUpdate);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteViajero(int viajeroId)
        {
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.DeleteAsync("https://localhost:5001/api/Viajeros/" + viajeroId))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                }
            }

            return RedirectToAction("Index");
        }


        public ViewResult AddViejero() => View();

        [HttpPost]
        public async Task<IActionResult> AddViajero(Viajero viajero)
        {
            Viajero newViajero = new Viajero();
            using (var httpClient = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(viajero), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync("https://localhost:5001/api/Viajeros", content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    newViajero = JsonConvert.DeserializeObject<Viajero>(apiResponse);
                }
            }
            return View(newViajero);
        }


        public ViewResult GetViajero() => View();

        [HttpPost]
        public async Task<IActionResult> GetViajero(int id)
        {
            Viajero viajeroResult = new Viajero();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://localhost:5001/api/Viajeros/" + id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    viajeroResult = JsonConvert.DeserializeObject<Viajero>(apiResponse);
                }
            }
            return View(viajeroResult);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}