﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ViajerosClient.Models;

namespace ViajerosClient.Controllers
{
    public class LugarController : Controller
    {
        private readonly ILogger<LugarController> _logger;

        public LugarController(ILogger<LugarController> logger) => _logger = logger;

        public async Task<IActionResult> IndexAsync()
        {
            List<Lugar> lugarList = new List<Lugar>();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://localhost:5001/api/Lugares"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    lugarList = JsonConvert.DeserializeObject<List<Lugar>>(apiResponse);
                }
            }
            return View(lugarList);

        }

        public async Task<IActionResult> UpdateLugar(int id)
        {
            Lugar lugar = new Lugar();

            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://localhost:5001/api/Lugares/" + id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    lugar = JsonConvert.DeserializeObject<Lugar>(apiResponse);
                }
            }
            return View(lugar);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateLugar(Lugar lugar)
        {
            Lugar viajeroUpdate = new Lugar();
            using (var httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri("https://localhost:5001");

                string serilized = JsonConvert.SerializeObject(lugar);
                var inputMessage = new HttpRequestMessage
                {
                    Content = new StringContent(serilized, Encoding.UTF8, "application/json")
                };
                inputMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                using (var response = await httpClient.PutAsync("/api/Lugares/" + lugar.Id, inputMessage.Content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    ViewBag.Result = "Success";
                    viajeroUpdate = JsonConvert.DeserializeObject<Lugar>(apiResponse);
                }
            }
            return View(viajeroUpdate);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteLugar(int lugarId)
        {
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.DeleteAsync("https://localhost:5001/api/Lugares/" + lugarId))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                }
            }

            return RedirectToAction("Index");
        }


        public ViewResult AddLugar() => View();

        [HttpPost]
        public async Task<IActionResult> AddLugar(Lugar lugar)
        {
            Lugar newLugar = new Lugar();
            using (var httpClient = new HttpClient())
            {
                StringContent content = new StringContent(JsonConvert.SerializeObject(lugar), Encoding.UTF8, "application/json");

                using (var response = await httpClient.PostAsync("https://localhost:5001/api/Lugares", content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    newLugar = JsonConvert.DeserializeObject<Lugar>(apiResponse);
                }
            }
            return View(newLugar);
        }


        public ViewResult GetLugar() => View();

        [HttpPost]
        public async Task<IActionResult> GetLugar(int id)
        {
            Lugar lugarResult = new Lugar();
            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://localhost:5001/api/Lugares/" + id))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    lugarResult = JsonConvert.DeserializeObject<Lugar>(apiResponse);
                }
            }
            return View(lugarResult);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}